﻿using System;

namespace ProtoripPattern
{
    /// <summary>
    /// Животное
    /// </summary>
    class Animal : IMyCloneable<Animal>, ICloneable
    {
        public string ClassName;

        public Animal(string className)
        {
            ClassName = className;
        }

        public object Clone()
        {
           return Copy();
        }

        public Animal Copy()
        {
            return new Animal(ClassName);
        }

        public override string ToString()
        {
            return $"Класс: {ClassName}";
        }       
    }
}

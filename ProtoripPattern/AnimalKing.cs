﻿using System;

namespace ProtoripPattern
{
    /// <summary>
    /// Вид животного
    /// </summary>
    class AnimalKing : AnimalGenus, IMyCloneable<AnimalKing>, ICloneable
    {
        public string KingName;

        public AnimalKing(string kingName, string genusName, string familyName, string squadName, string className)
            : base(genusName, familyName, squadName, className)
        {
            KingName = kingName;
        }

        public new object Clone()
        {
            return Copy();
        }

        public new AnimalKing Copy()
        {
            return new AnimalKing(KingName, GenusName, FamilyName, SquadName, ClassName);
        }

        public override string ToString()
        {
            return $"Класс: {ClassName} | Отряд: {SquadName} | Семейство: {FamilyName} | Род: {GenusName} | Вид: {KingName}";
        }
    }
}

﻿namespace ProtoripPattern
{
    interface IMyCloneable<T>
    {
        T Copy();
    }
}

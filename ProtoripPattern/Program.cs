﻿using System;

namespace ProtoripPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Есть вложенный файл Классификация животного мира "animals-classifier.jpg - для наглядности" 
             * Из классификации взяты несколько наследников:
             * классификации животного мира по "Классу" назначен класс Animal
             * классификации класса по "Отряду" назначен класс AnimalSquad
             * классификации отряда по "Семейству" назначен класс AnimalFamily
             * классификации семейства по "Роду" назначен класс AnimalGenus 
             * классификации рода по "Виду" назначен класс AnimalKing */

            Animal mammals = new Animal("Млекопитающие");
            AnimalSquad primates = new AnimalSquad("Приматы", "Млекопитающие");
            AnimalFamily canids = new AnimalFamily("Псовые", "Хищные", "Млекопитающие");
            AnimalGenus dog = new AnimalGenus("Собака", "Псовые", "Хищные", "Млекопитающие");
            AnimalKing wolf = new AnimalKing("Волк", "Собака", "Псовые", "Хищные", "Млекопитающие");

            var insects = mammals.Copy();
            insects.ClassName = "Насекомые";

            var predatory = primates.Copy();
            predatory.SquadName = "Хищные";

            var feline = canids.Copy();
            feline.FamilyName = "Кошачьи";

            var fox = dog.Copy();
            fox.GenusName = "Лисица";

            var jackal = wolf.Copy();
            jackal.KingName = "Шакал"; 

            var birds = (Animal)insects.Clone();
            birds.ClassName = "Птицы";

            var rodents = (AnimalSquad)predatory.Clone();
            rodents.SquadName = "Грызуны";

            var hare = (AnimalFamily)feline.Clone();
            hare.FamilyName = "Заячьи";

            var coyote = (AnimalGenus)fox.Clone();
            coyote.GenusName = "Койот";

            var arcticFox = (AnimalKing)jackal.Clone();
            arcticFox.KingName = "Песец";

            Console.WriteLine("Созданные объекты\n");
            Console.WriteLine(mammals);
            Console.WriteLine(primates);
            Console.WriteLine(canids);
            Console.WriteLine(dog);
            Console.WriteLine(wolf);

            Console.WriteLine("\nКлонирование объектов с использованием IMyCloneable\n");
            Console.WriteLine(insects);
            Console.WriteLine(predatory);
            Console.WriteLine(feline);
            Console.WriteLine(fox);
            Console.WriteLine(jackal);

            Console.WriteLine("\nКлонирование объектов с использованием ICloneable\n");
            Console.WriteLine(birds);
            Console.WriteLine(rodents);
            Console.WriteLine(hare);
            Console.WriteLine(coyote);
            Console.WriteLine(arcticFox);

            Console.ReadLine();
        }
    }
}

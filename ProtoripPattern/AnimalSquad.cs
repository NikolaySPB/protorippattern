﻿using System;

namespace ProtoripPattern
{
    /// <summary>
    /// Отряд животных
    /// </summary>
    class AnimalSquad : Animal, IMyCloneable<AnimalSquad>, ICloneable
    {
        public string SquadName;

        public AnimalSquad(string squadName, string className)
            : base(className)
        {
            SquadName = squadName;
        }

        public new object Clone()
        {
            return Copy();
        }

        public new AnimalSquad Copy()
        {
            return new AnimalSquad(SquadName, ClassName);
        }

        public override string ToString()
        {
            return $"Класс: {ClassName} | Отряд: {SquadName}";
        }
    }
}

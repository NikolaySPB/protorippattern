﻿using System;

namespace ProtoripPattern
{
    /// <summary>
    /// Семейство животных
    /// </summary>
    class AnimalFamily : AnimalSquad, IMyCloneable<AnimalFamily>, ICloneable
    {
        public string FamilyName;

        public AnimalFamily(string familyName, string squadName, string className) 
            : base(squadName, className)
        {
            FamilyName = familyName;
        }

        public new object Clone()
        {
            return Copy();
        }

        public new AnimalFamily Copy()
        {
            return new AnimalFamily(FamilyName, SquadName, ClassName);
        }

        public override string ToString()
        {
            return $"Класс: {ClassName} | Отряд: {SquadName} | Семейство: {FamilyName}";
        }        
    }
}

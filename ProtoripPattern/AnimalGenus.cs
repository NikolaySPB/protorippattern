﻿using System;

namespace ProtoripPattern
{
    /// <summary>
    /// Род животных
    /// </summary>
    class AnimalGenus : AnimalFamily, IMyCloneable<AnimalGenus>, ICloneable
    {
        public string GenusName;

        public AnimalGenus(string genusName, string familyName, string squadName, string className) 
            : base(familyName, squadName, className)
        {
            GenusName = genusName;
        }

        public new object Clone()
        {
            return Copy();
        }

        public new AnimalGenus Copy()
        {
            return new AnimalGenus(GenusName, FamilyName, SquadName, ClassName);
        }

        public override string ToString()
        {
            return $"Класс: {ClassName} | Отряд: {SquadName} | Семейство: {FamilyName} | Род: {GenusName}";
        }
    }
}
